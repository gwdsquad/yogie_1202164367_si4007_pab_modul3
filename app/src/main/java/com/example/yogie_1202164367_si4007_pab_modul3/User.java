package com.example.yogie_1202164367_si4007_pab_modul3;

public class User {
    private String name;
    private String work;
    private final int avatar;

    public User(String name, String work, int avatar) {
        this.name = name;
        this.work = work;
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public String getWork() {
        return work;
    }

    public int getAvatar() {
        return avatar;
    }
}

